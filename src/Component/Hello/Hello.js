const Hello = ({ route }) => {
  return (
    <div>
      <h2>Hello, {route}</h2>
    </div>
  );
};

export default Hello;
